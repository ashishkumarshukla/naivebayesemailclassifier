import os
import io
from pandas import DataFrame
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB


def readFiles(path):
    for root, dirnames, filenames in os.walk(path):
        for filename in filenames:
            path = os.path.join(root, filename)
            inbody=False
            lines = []
            f = io.open(path, 'r', encoding='latin1')
            for line in f:
                if inbody:
                    lines.append(line)
                elif line == '\n':
                    inbody = True
            f.close()
            message = '\n'.join(lines)
            yield path, message


def dataFrameFromDir(path, classification):
    rows = []
    index = []
    for filename, message in readFiles(path):
        rows.append({'message': message, 'class': classification})
        index.append(filename)

    return DataFrame(rows, index=index)


data = DataFrame({'message': [], 'class': []})
data = data.append(dataFrameFromDir('D:/ASHISH/emails/spam', 'spam'), sort=False)
data = data.append(dataFrameFromDir('D:/ASHISH/emails/ham', 'ham'), sort=False)

vectorizer = CountVectorizer()
counts = vectorizer.fit_transform(data['message'].values)

classifier = MultinomialNB()
targets = data['class'].values
classifier.fit(counts, targets)

test = ['free shit', 'you kidding bob', 'Free food', 'free visa', 'No Visa required for US']
test_counts = vectorizer.transform(test)
predictions = classifier.predict(test_counts)
print(predictions)
